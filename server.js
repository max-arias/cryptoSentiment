// get tweets filtered by crypto
// analyze their sentiment

// https://github.com/ttezel/twit#twit
// https://github.com/thisandagain/sentiment

// https://github.com/fnielsen/afinn/blob/master/afinn/data/AFINN-en-165.txt
// https://github.com/fnielsen/afinn/blob/master/afinn/data/AFINN-emoticon-8.txt

require('dotenv').config()

const Twit = require('twit')
const sentiment = require('sentiment');
const colors = require('colors');

const T = new Twit({
    consumer_key: process.env.CONSUMER_KEY,
    consumer_secret: process.env.CONSUMER_SECRET,
    access_token: process.env.ACCESS_TOKEN,
    access_token_secret: process.env.ACCESS_TOKEN_SECRET,
    timeout_ms: 60 * 1000,  // optional HTTP request timeout to apply to all requests.
});

const stream = T.stream('statuses/filter', { track: ['xrp', 'ripple', '$xrp', 'btc', 'crypto', 'bitcoin'] });

stream.on('tweet', function (tweet) {
    const calculatedSentiment = sentiment(tweet.text);

    if (calculatedSentiment.score > 0) {
        console.log(colors.green(calculatedSentiment.score) + ' | ' + tweet.text);
    } else if (calculatedSentiment.score < 0) {
        console.log(colors.red(calculatedSentiment.score) + ' | ' + tweet.text);
    } else {
        console.log(colors.gray(calculatedSentiment.score) + ' | ' + tweet.text);
    }
});